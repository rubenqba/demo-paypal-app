package com.github.rubenqba.paypal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPaypalAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPaypalAppApplication.class, args);
	}
}
